import { Buffer } from 'buffer';
import { validate as isValidUUID } from 'uuid';
import { Model, Data } from '@avstantso/node-or-browser-js--model-core';

import { APIExec, API } from '@types';
import MESSAGES from '@messages';
import { SocketLib } from '@socketLib';
import { cutAndValidate, shortModelName } from '@utils';

import { ExecutorFactory, ExecDataOpts, isExecutor } from './api-exec';
import { Generics } from '@avstantso/node-or-browser-js--utils';

export namespace AbstractApi {
  export interface Overload {
    <
      TModel extends Model.Structure,
      TSocketLib extends SocketLib.Abstract.Structure
    >(
      model: Model.Declaration<TModel>,
      executor: APIExec.Executor<TModel, TSocketLib>
    ): API.Abstract.Factory<TModel, TSocketLib>;

    <
      TModel extends Model.Structure,
      TSocketLib extends SocketLib.Abstract.Structure
    >(
      model: Model.Declaration<TModel>,
      apiOptions: TSocketLib['Options'] &
        APIExec.Executor.Options &
        APIExec.Executor.Options.SocketLib<TSocketLib>
    ): API.Abstract.Factory<TModel, TSocketLib>;
  }
}

export const AbstractApi: AbstractApi.Overload = <
  TModel extends Model.Structure,
  TSocketLib extends SocketLib.Abstract.Structure
>(
  model: Model.Declaration<TModel>,
  param: unknown
): API.Abstract.Factory<TModel, TSocketLib> => {
  //#region internal types aliases
  type Config = TSocketLib['Config'];
  type Insert = TModel['Insert'];
  type Update = TModel['Update'];
  type Select = TModel['Select'];
  type SelectOptions = Model.Select.Options<TModel>;
  type SelectArray = Select[];
  type Search = TModel['Condition'];
  type PGResult = Model.Pagination.Result<TModel>;
  type PGResultObj = Model.Pagination.Result.AsObject<TModel>;
  //#endregion

  const Executor = ExecutorFactory<TSocketLib>();

  const executor: APIExec.Executor<TModel, TSocketLib> = isExecutor<
    TModel,
    TSocketLib
  >(param)
    ? param
    : Executor(
        model,
        param as TSocketLib['Options'] &
          APIExec.Executor.Options &
          APIExec.Executor.Options.SocketLib<TSocketLib>
      );

  const { getUrl, exec, clientChecks, cases } = executor;

  function ListParams(
    search: Search,
    pagination?: Model.Pagination
  ): Model.Select.Options.Serialized {
    return {
      ...(!pagination ? {} : { pagination: Generics.Cast.To(pagination) }),
      ...(!search
        ? {}
        : {
            search: Buffer.from(JSON.stringify(search)).toString('base64'),
          }),
    };
  }

  const listSearch: API.Abstract.Readable.List.Search<TModel, TSocketLib> = (
    search,
    config?
  ) =>
    exec<SelectArray, SelectOptions>(
      listSearch,
      () => (search ? [config, { search }] : config),
      ({ socketLib, url, data: { search }, config }) =>
        socketLib.get<Data<SelectArray>>(url, ListParams(search), config),
      { defaultValue: [], ...ExecDataOpts }
    );

  const list: API.Abstract.Readable.List<TModel, TSocketLib> = (config?) =>
    listSearch(undefined, config);

  const listPagesSearch: API.Abstract.Readable.List.Pages.Search<
    TModel,
    TSocketLib
  > = (pagination, search, config?) =>
    exec<PGResult, SelectOptions, PGResultObj>(
      listPagesSearch,
      () => [config, { pagination, ...(search ? { search } : {}) }],
      ({ socketLib, url, data: { pagination, search }, config }) =>
        socketLib.get<PGResultObj, Model.Select.Options.Serialized>(
          url,
          ListParams(search, pagination),
          config
        ),
      {
        defaultValue: [undefined, []],
        needTransform: true,
        getData: (resData: PGResultObj) => [resData?.total, resData?.data],
      }
    );

  const listPages: API.Abstract.Readable.List.Pages<TModel, TSocketLib> = (
    pagination,
    config?
  ) => listPagesSearch(pagination, undefined, config);

  listPages.search = listPagesSearch;
  list.search = listSearch;
  list.pages = listPages;

  const one: API.Abstract.Readable.One<TModel, TSocketLib> = (id, config?) =>
    exec<Select>(
      one,
      ({ paramsError }) => {
        if (clientChecks) {
          if (!id) throw paramsError(MESSAGES.idNotSet);

          if (!isValidUUID(id))
            throw paramsError(`${MESSAGES.invalidUUIDFormatForId} "${id}"`);
        }

        return getUrl(shortModelName(model, cases?.model), id);
      },
      ({ socketLib, url }) =>
        socketLib.get<Data<Select>>(url, undefined, config),
      ExecDataOpts
    );

  const insert: API.Abstract.Writable.Insert<TModel, TSocketLib> = (
    entity,
    config?
  ) =>
    exec<Select, Insert>(
      insert,
      (erc) => {
        if (clientChecks) {
          if (!entity) throw erc.paramsError(MESSAGES.dataNotSet);
        }

        const data = cutAndValidate<Insert | Select, Insert>(
          entity,
          model.Schema.Insert,
          erc,
          clientChecks
        );

        return [config, data];
      },
      ({ socketLib, url, data }) =>
        socketLib.put<Data<Select>, Data<Insert>>(url, { data }, config),
      ExecDataOpts
    );

  const update: API.Abstract.Writable.Update<TModel, TSocketLib> = (
    entity,
    config?
  ) =>
    exec<Select, Update>(
      update,
      (erc) => {
        if (clientChecks) {
          if (!entity) throw erc.paramsError(MESSAGES.dataNotSet);
        }

        const data = cutAndValidate<Update | Select, Update>(
          entity,
          model.Schema.Update,
          erc,
          clientChecks
        );

        return [config, data];
      },
      ({ socketLib, url, data }) =>
        socketLib.post<Data<Select>, Data<Update>>(url, { data }, config),
      ExecDataOpts
    );

  const _delete: API.Abstract.Writable.Delete<TSocketLib> = (id, config?) =>
    exec<Model.ID>(
      'delete',
      ({ paramsError }) => {
        if (clientChecks) {
          if (!id) throw paramsError(MESSAGES.idNotSet);

          if (!isValidUUID(id))
            throw paramsError(`${MESSAGES.invalidUUIDFormatForId} "${id}"`);
        }

        return config;
      },
      ({ socketLib, url }) => socketLib.delete<Data<Model.ID>>(url, id, config),
      ExecDataOpts
    );

  const Readable = () => ({ list, one });
  const Writable = () => ({ insert, update, delete: _delete });
  const ReadableWritable = () => ({ ...Readable(), ...Writable() });

  return {
    Readable,
    Writable,
    ReadableWritable,
  };
};

import Bluebird from 'bluebird';
import { JSONSchema7 } from 'json-schema';

import { Cases, Generics, JS } from '@avstantso/node-or-browser-js--utils';
import {
  Model,
  Data,
  DataDecorator,
  validateBySchema,
} from '@avstantso/node-or-browser-js--model-core';

import { APIExec } from '@types';
import MESSAGES from '@messages';
import { SocketLib } from '@socketLib';
import {
  TransformData,
  defaultDataDecorator,
  shortMethodName,
  shortModelName,
} from '@utils';
import { ParamsError } from '@classes';

export const ExecDataOpts = {
  needTransform: true,
  getData: <T>(resData: Data<T>) => resData?.data,
};

export function isExecutor<
  TModel extends APIExec.Executor.Model = APIExec.Executor.Model,
  TSocketLib extends SocketLib.Abstract.Structure = SocketLib.Abstract.Structure
>(candidate: unknown): candidate is APIExec.Executor<TModel, TSocketLib> {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    'getSocketLib' in candidate &&
    'transformData' in candidate &&
    'getUrl' in candidate &&
    'exec' in candidate
  );
}

export const ExecutorFactory: APIExec.Executor.Factory = <
  TSocketLib extends SocketLib.Abstract.Structure
>() => {
  const Executor: APIExec.Executor.Overload<TSocketLib> = <
    TModel extends APIExec.Executor.Model
  >(
    modelOrName: any,
    apiOptions: TSocketLib['Options'] &
      APIExec.Executor.Options &
      APIExec.Executor.Options.SocketLib<TSocketLib>
  ): APIExec.Executor<TModel, TSocketLib> => {
    const model: Model.Named = JS.is.string(modelOrName)
      ? { name: modelOrName }
      : JS.is.function(modelOrName)
      ? { name: modelOrName.name }
      : (modelOrName as Model.Named);

    const { clientChecks, GetSocketLib, getCatcher, getMasterUrl, cases } =
      apiOptions;
    const getSocketLib = GetSocketLib(apiOptions);

    const transformData = TransformData(
      model,
      apiOptions.decorator || DataDecorator(defaultDataDecorator)
    );

    const getUrl = (...relUrlParts: [string, ...string[]]) =>
      (getMasterUrl ? [getMasterUrl(), ...relUrlParts] : relUrlParts).join('/');

    const exec: APIExec.Exec<TSocketLib> = <
      TResult,
      TInData = never,
      TResponse = Data<TResult>
    >(
      method: string | Function,
      ...execParams: any[]
    ) => {
      const hasPrepare = JS.is.function(execParams[1]);

      const prepare: APIExec.Exec.Prepare<TSocketLib, TInData> =
        hasPrepare && execParams[0];

      const query: APIExec.Exec.Query<TSocketLib, TInData, TResponse> =
        execParams[0 + (hasPrepare ? 1 : 0)];
      const options: APIExec.Exec.Options<TResult, TResponse> =
        execParams[1 + (hasPrepare ? 1 : 0)];

      const { defaultValue, needTransform, getData } = options || {};

      function paramsError(message: string) {
        return new ParamsError(model.name, method, message);
      }

      function validate(data: TInData, schema: JSONSchema7) {
        const { isValid, errors } = validateBySchema(data, schema);
        if (isValid) return;

        const e = new ParamsError(
          model.name,
          method,
          MESSAGES.dataDoesNotMatchSchema
        );

        e.details = errors;

        throw e;
      }

      const strings = {
        model: shortModelName(model, cases?.model),
        method: shortMethodName(method, cases?.method),
      };

      return Bluebird.resolve(
        hasPrepare
          ? prepare({ ...strings, paramsError, validateBySchema: validate })
          : getUrl(strings.model, strings.method)
      ).then((params: any) => {
        const isParamsArr = Array.isArray(params);
        const hasUrl = JS.is.string(isParamsArr ? params[0] : params);

        const url: string = hasUrl
          ? isParamsArr
            ? params[0]
            : params
          : getUrl(strings.model);
        const config: TSocketLib['Config'] = isParamsArr
          ? params[hasUrl ? 1 : 0]
          : params;
        const data: TInData = isParamsArr && params[hasUrl ? 2 : 1];

        const socketLib = getSocketLib(getCatcher(model.name, method));

        return Bluebird.resolve(query({ socketLib, url, config, data }))
          .then((res) => (getData ? getData(res) : Generics.Cast.To(res)))
          .then((data) =>
            undefined === defaultValue ? data : data || defaultValue
          )
          .then((data) =>
            !needTransform
              ? data
              : !Array.isArray(data)
              ? transformData(data)
              : Model.Pagination.Result.Is(data)
              ? [data[0], data[1].map(transformData)]
              : data.map(transformData)
          );
      });
    };

    return {
      cases,
      getSocketLib,
      clientChecks,
      transformData,
      getUrl,
      exec,
    };
  };

  return Executor;
};

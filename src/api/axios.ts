import { Model } from '@avstantso/node-or-browser-js--model-core';

import { APIExec, API } from '@types';
import { SocketLib } from '@socketLib';

import { AbstractApi } from './abstract';

export function AxiosApi<TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  executor: APIExec.Executor<TModel, SocketLib.Axios.Structure>
): API.Axios.Factory<TModel> {
  return AbstractApi<TModel, SocketLib.Axios.Structure>(model, executor);
}

import axios, { AxiosInstance } from 'axios';
import * as Types from './types';

export type Authorized = Types.Authorized;

const authorizedAI = (ai: AxiosInstance, auth: Authorized) => {
  const { baseURL, timeout, headers } = ai.defaults;
  const { jwt } = auth;

  return axios.create({
    baseURL,
    timeout,
    headers: { ...headers, Authorization: `Bearer ${jwt}` },
  });
};

export const Authorized = { AI: authorizedAI };

import { JS } from '@avstantso/node-or-browser-js--utils';
import { ModelMethodError } from '@avstantso/node-or-browser-js--model-core';

export namespace MethodError {
  export type Reporter<T extends MethodError> = {
    (message: string, internal?: Error): T;

    readonly model: string;
    readonly method: string;
  };
}

export class MethodError extends ModelMethodError {
  constructor(
    model: string | Function,
    method: string | Function,
    message: string,
    internal?: Error
  ) {
    super(model, method, message, internal);

    Object.setPrototypeOf(this, MethodError.prototype);
  }

  static is(error: unknown): error is MethodError {
    return JS.is.error(this, error);
  }

  protected static makeReporter<T extends MethodError>(
    model: string | Function,
    method: string | Function,
    make: (message: string, internal?: Error) => T
  ): MethodError.Reporter<T> {
    const report = (message: string, internal?: Error) =>
      make(message, internal);

    report.model = JS.is.function(model) ? model.name : model;
    report.method = JS.is.function(method) ? method.name : method;

    return report;
  }
}

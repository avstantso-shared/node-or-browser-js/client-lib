import { JS } from '@avstantso/node-or-browser-js--utils';
import { MethodError } from './MethodError';

/**
 * @summary Client errors without server requests
 */
export class ParamsError extends MethodError {
  constructor(
    model: string | Function,
    method: string | Function,
    message: string,
    internal?: Error
  ) {
    super(model, method, message, internal);

    Object.setPrototypeOf(this, ParamsError.prototype);
  }

  static is(error: unknown): error is ParamsError {
    return JS.is.error(this, error);
  }

  static reporter(
    model: string | Function,
    method: string | Function
  ): MethodError.Reporter<ParamsError> {
    return MethodError.makeReporter(
      model,
      method,
      (message: string, internal?: Error) =>
        new ParamsError(model, method, message, internal)
    );
  }
}

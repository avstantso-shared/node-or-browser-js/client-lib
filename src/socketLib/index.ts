import type * as Types from '@types';
import { GetAxiosSocketLib } from './axios';

export namespace SocketLib {
  export namespace Abstract {
    export type Structure = Types.SocketLib.Abstract.Structure;

    export type GetInternal<TSocketLib extends Structure> =
      Types.SocketLib.Abstract.GetInternal<TSocketLib>;

    export type Get<TSocketLib extends Structure> =
      Types.SocketLib.Abstract.Get<TSocketLib>;
  }
  export type Abstract<TSocketLib extends Abstract.Structure> =
    Types.SocketLib.Abstract.SocketLib<TSocketLib>;

  export type Structure = Abstract.Structure;

  export namespace Axios {
    export type Structure = Types.SocketLib.Axios.Structure;
    export type Options = Structure['Options'];
    export type GetInternal = Types.SocketLib.Axios.GetInternal;
    export type Get = Types.SocketLib.Axios.Get;
  }
  export type Axios = Types.SocketLib.Axios.SocketLib;
}

export const SocketLib = { Axios: GetAxiosSocketLib };

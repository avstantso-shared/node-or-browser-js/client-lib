import type { JSONSchema7 } from 'json-schema';
import type { Cases } from '@avstantso/node-or-browser-js--utils';
import type {
  Model,
  Data,
  DataDecorator,
} from '@avstantso/node-or-browser-js--model-core';
import { ParamsError } from '@classes';
import type * as SocketLib from '../socketLib';

export type Catcher = (e: Error) => Promise<never>;
export type GetCatcher = (
  modelUrl: string | Function,
  method: string | Function
) => Catcher;

export namespace Exec {
  export interface Options<TResult, TResponse> {
    defaultValue?: TResult;
    needTransform?: boolean;
    getData?(res: TResponse): TResult;
  }

  export namespace Prepare {
    export interface Context<TInData> {
      readonly model: string;
      readonly method: string;
      paramsError(message: string): ParamsError;
      validateBySchema(data: TInData, schema: JSONSchema7): void | never;
    }

    export namespace Result {
      export type Item<
        TSocketLib extends SocketLib.Abstract.Structure,
        TInData
      > =
        | string
        | TSocketLib['Config']
        | [string, TSocketLib['Config']?, TInData?]
        | [TSocketLib['Config'], TInData?];
    }

    export type Result<
      TSocketLib extends SocketLib.Abstract.Structure,
      TInData,
      TItem extends Prepare.Result.Item<
        TSocketLib,
        TInData
      > = Prepare.Result.Item<TSocketLib, TInData>
    > = TItem | Promise<TItem>;
  }

  export type Prepare<
    TSocketLib extends SocketLib.Abstract.Structure,
    TInData
  > = (
    context: Prepare.Context<TInData>
  ) => Prepare.Result<TSocketLib, TInData>;

  export namespace Query {
    export interface Context<
      TSocketLib extends SocketLib.Abstract.Structure,
      TInData
    > {
      socketLib: SocketLib.Abstract.SocketLib<TSocketLib>;
      url?: string;
      config?: TSocketLib['Config'];
      data?: TInData;
    }
  }

  export type Query<
    TSocketLib extends SocketLib.Abstract.Structure,
    TInData,
    TResponse
  > = (context: Query.Context<TSocketLib, TInData>) => Promise<TResponse>;
}

export type Exec<TSocketLib extends SocketLib.Abstract.Structure> = {
  <TResult = void, TInData = never, TResponse = Data<TResult>>(
    method: string | Function,
    prepare: Exec.Prepare<TSocketLib, TInData>,
    query: Exec.Query<TSocketLib, TInData, TResponse>,
    options?: Exec.Options<TResult, TResponse>
  ): Promise<TResult>;

  <TResult = void, TInData = never, TResponse = Data<TResult>>(
    method: string | Function,
    query: Exec.Query<TSocketLib, TInData, TResponse>,
    options?: Exec.Options<TResult, TResponse>
  ): Promise<TResult>;
};

export namespace Executor {
  export type Model = Partial<Pick<Model.Structure, 'Select'>>;

  export namespace Options {
    export interface SocketLib<
      TSocketLib extends SocketLib.Abstract.Structure
    > {
      GetSocketLib: SocketLib.Abstract.Get<TSocketLib>;
      getCatcher: GetCatcher;
    }

    export type UrlCases = Partial<Record<'model' | 'method', Cases.Union>>;
  }

  export interface Options {
    decorator?: DataDecorator;
    getMasterUrl?: () => string;
    clientChecks?: boolean;
    cases?: Options.UrlCases;
  }

  export type Overload<TSocketLib extends SocketLib.Abstract.Structure> = {
    <TModel extends Model.Structure>(
      model: Model.Declaration<TModel>,
      apiOptions: TSocketLib['Options'] &
        Options &
        Options.SocketLib<TSocketLib>
    ): Executor<TModel, TSocketLib>;

    <TModel extends Executor.Model = unknown>(
      modelName: string,
      apiOptions: TSocketLib['Options'] &
        Options &
        Options.SocketLib<TSocketLib>
    ): Executor<TModel, TSocketLib>;

    <TModel extends Executor.Model = unknown>(
      modelFactory: Function,
      apiOptions: TSocketLib['Options'] &
        Options &
        Options.SocketLib<TSocketLib>
    ): Executor<TModel, TSocketLib>;
  };

  export type Factory = <
    TSocketLib extends SocketLib.Abstract.Structure
  >() => Overload<TSocketLib>;
}

export interface Executor<
  TModel extends Executor.Model,
  TSocketLib extends SocketLib.Abstract.Structure
> extends Pick<Executor.Options, 'cases'> {
  getSocketLib: SocketLib.Abstract.GetInternal<TSocketLib>;
  readonly clientChecks: boolean;
  transformData(data: any): TModel['Select'];
  getUrl(...relUrlParts: [string, ...string[]]): string;
  exec: Exec<TSocketLib>;
}

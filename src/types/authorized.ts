export interface Authorized {
  readonly jwt: string;
}

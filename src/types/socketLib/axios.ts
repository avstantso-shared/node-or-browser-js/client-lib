import type { AxiosError, AxiosInstance, AxiosRequestConfig } from 'axios';
import type * as Abstract from './abstract';

export interface Structure extends Abstract.Structure {
  Options: {
    readonly axiosInstance: AxiosInstance;
  };
  Config: AxiosRequestConfig;
  Error: AxiosError;
}

export type SocketLib = Abstract.SocketLib<Structure>;
export type GetInternal = Abstract.GetInternal<Structure>;
export type Get = Abstract.Get<Structure>;

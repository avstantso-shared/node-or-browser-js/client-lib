import type { JSONSchema7 } from 'json-schema';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import { cutBySchema } from '@avstantso/node-or-browser-js--model-core';

import { APIExec } from '@types';

export function cutAndValidate<TFrom, TTo>(
  entity: TFrom,
  schema: JSONSchema7,
  erc: APIExec.Exec.Prepare.Context<TTo>,
  clientChecks?: boolean
): TTo {
  let r: TTo;
  try {
    r = cutBySchema<TFrom, TTo>(entity, schema);
  } catch {
    r = Generics.Cast(entity);
  }

  clientChecks && erc.validateBySchema(r, schema);

  return r;
}

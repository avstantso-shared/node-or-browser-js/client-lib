import { DataDecorator } from '@avstantso/node-or-browser-js--model-core';

export const defaultDataDecorator: DataDecorator.Setup = (dd) => dd.Dates;

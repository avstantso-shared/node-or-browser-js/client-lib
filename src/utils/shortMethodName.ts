import { Cases, JS } from '@avstantso/node-or-browser-js--utils';

export function shortMethodName(
  method: string | Function,
  methodCase?: Cases.Union
): string {
  const m = JS.is.function(method) ? method.name : method;

  return methodCase ? Cases.Resolve(methodCase)(m) : m;
}

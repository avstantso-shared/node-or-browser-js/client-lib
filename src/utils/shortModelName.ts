import { Cases } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

export function shortModelName(
  model: string | Model.Named,
  modelCase: Cases.Union = 'camel'
): string {
  const m = Model.Named.Extract(model).split('.').pop();

  return Cases.Resolve(modelCase)(m);
}

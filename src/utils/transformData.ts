import {
  AtomicObjects,
  Generics,
  JS,
} from '@avstantso/node-or-browser-js--utils';
import {
  DataDecorator,
  Model,
} from '@avstantso/node-or-browser-js--model-core';

export function TransformData<T = any>(
  model: Model.Named,
  decorator: DataDecorator<any>
): T {
  const transformData = (data: any): any => {
    if (!data || !JS.is.object(data) || AtomicObjects.is(data)) return data;

    if (Array.isArray(data))
      for (let i = 0; i < data.length; i++) data[i] = transformData(data[i]);
    else {
      data = decorator(data);

      Object.entries(data).forEach(([k, v]) =>
        JS.set.safe(data, k, transformData(v))
      );
    }

    return data;
  };

  return Generics.Cast.To(transformData);
}
